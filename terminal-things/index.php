<!DOCTYPE html>
<html>
	<head>
		<title>Terminal Things|Redlolz</title>
		<link rel="stylesheet" href="/stylesheets/main-style.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	</head>

	<body>
		<header>
			<?php
				include $_SERVER['DOCUMENT_ROOT'].'/header.php';
			?>
		</header>

		<div class="logo">
			<pre class="ascii_art">
 _____ _____ ____  __  __ ___ _   _    _    _     
|_   _| ____|  _ \|  \/  |_ _| \ | |  / \  | |    
  | | |  _| | |_) | |\/| || ||  \| | / _ \ | |    
  | | | |___|  _ <| |  | || || |\  |/ ___ \| |___ 
  |_| |_____|_| \_\_|  |_|___|_| \_/_/   \_\_____|
			</pre>
		</div>
		<div class="logo">
			<pre class="ascii_art">
 _____ _   _ ___ _   _  ____ ____  
|_   _| | | |_ _| \ | |/ ___/ ___| 
  | | | |_| || ||  \| | |  _\___ \ 
  | | |  _  || || |\  | |_| |___) |
  |_| |_| |_|___|_| \_|\____|____/ 
			</pre>
		</div>
		
		<main>
			<ul>
				<?php
					$articles_directory = 'articles';
					$directory = glob($articles_directory.'/*');
					foreach(array_reverse($directory) as $file):
						$filename = explode('/', $file);
				?>
				
				<li><?php echo "<a href='article.php?article=" . $filename[1] . "'>" . $filename[1] . "</a>"; ?></li>

				<?php endforeach; ?>
			</ul>
		</main>
	</body>
</html>
