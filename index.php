<!DOCTYPE html>
<html>
	<head>
		<title>Home|Redlolz</title>
		<link rel="stylesheet" href="/stylesheets/main-style.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	</head>

	<body>
		<header>
<?php
include $_SERVER['DOCUMENT_ROOT'].'/header.php';
?>
		</header>
		<div class="logo">
			<pre class="ascii_art">
 ____  _____ ____  _     ___  _     _____
|  _ \| ____|  _ \| |   / _ \| |   |__  /
| |_) |  _| | | | | |  | | | | |     / /
|  _ <| |___| |_| | |__| |_| | |___ / /_
|_| \_\_____|____/|_____\___/|_____/____|
			</pre>
		</div>
<?php
if ($browser_term) {
?>

<main class="index-sections">
	<!--Blog-->
	<a href="/blog">
		<h2 class="index-section-title">Blog</h2>
		<p class="index-section-subtitle">[Just random stuff]</p>
	</a>		

	<!--Projects-->
	<a href="/projects">
		<h2 class="index-section-title">Projects</h2>
		<p class="index-section-subtitle">[Projects Ive worked on]</p>
	</a>

	<!--Terminal Things-->
	<a href="/terminal-things">
		<h2 class="index-section-title">Terminal Things</h2>
		<p class="index-section-subtitle">[Things about terminals]</p>
	</a>
</main>

<?php
}
else {
?>


<main class="index-sections">
	<!--Blog-->
	<a href="/blog">
		<div class="index-section">
			<h2 class="index-section-title">Blog</h2>
			<p class="index-section-subtitle">[Just random stuff]</p>
		</div>
	</a>		

	<!--Projects-->
	<a href="/projects">
		<div class="index-section">
			<h2 class="index-section-title">Projects</h2>
			<p class="index-section-subtitle">[Projects I've worked on]</p>
		</div>
	</a>

	<!--Terminal Things-->
	<a href="/terminal-things">
		<div class="index-section">
			<h2 class="index-section-title">Terminal Things</h2>
			<p class="index-section-subtitle">[Things about terminals]</p>
		</div>
	</a>
</main>


<?php
}
?>
	</body>
</html>
