<?php
header("Content-type: text/css");
$files = glob('gifs/*.gif');
$random_bg = $files[array_rand($files)];
?>
body {
	display: flex;
	justify-content: center;
	flex-direction: column;
	text-align: center;
	background: rgba(0, 0, 0, 0) url(<?=$random_bg?>) no-repeat fixed center center / cover
}

#main {
	padding: 30vh 0;
}

#search {
	width: 75%;
	color: #eee;
	font-family: 'UnifontMedium';
	font-size: 6vh;
	background-color: #444;
	outline: none;
	border: 0;
	padding: 1vh;
}

#clock {
	margin-bottom: 4vh;
	margin-left: auto;
	margin-right: auto;
	width: 80%;
	font-family: 'UnifontMedium';
	font-weight: normal;
	font-style: normal;
	font-size: 20vh;
	background: rgba(200, 200, 200, 0.65);
	color: #fff;
}

@media screen and (max-width: 500px) {
	#clock {
		width: 90%;
		font-size: 12vh;
	}
}
