<!DOCTYPE html>
<html>
	<head>
		<title>Blog|Redlolz</title>
		<link rel="stylesheet" href="/stylesheets/main-style.css">
		<link rel="stylesheet" href="/stylesheets/article-style.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	</head>

	<body>
		<header>
			<?php
				include $_SERVER['DOCUMENT_ROOT'].'/header.php';
			?>
		</header>

		<article>
			<?php 
				require '../Parsedown.php';
				$Parsedown = new Parsedown();
				$mdfile = str_replace('/', '', $_GET['article']);
				$file_location = $_SERVER['DOCUMENT_ROOT'] . '/blog/articles/' . $mdfile;
				$myfile = fopen($file_location, 'r') or die('Unable to open file!');
				echo $Parsedown->text(fread($myfile, filesize($file_location)));
			?>
		</article>
	</body>
</html>
