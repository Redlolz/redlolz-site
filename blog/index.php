<!DOCTYPE html>
<html>
	<head>
		<title>Blog|Redlolz</title>
		<link rel="stylesheet" href="/stylesheets/main-style.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	</head>

	<body>
		<header>
			<?php
				include $_SERVER['DOCUMENT_ROOT'].'/header.php';
			?>
		</header>

		<div class="logo">
			<pre class="ascii_art">
 ____  _     ___   ____
| __ )| |   / _ \ / ___|
|  _ \| |  | | | | |  _
| |_) | |__| |_| | |_| |
|____/|_____\___/ \____|
			</pre>
		</div>
		
		<main>
			<ul>
				<?php
					$articles_directory = 'articles';
					$directory = glob($articles_directory.'/*');
					foreach(array_reverse($directory) as $file):
						$filename = explode('/', $file);
				?>
				
				<li><?php echo "<a href='article.php?article=" . $filename[1] . "'>" . $filename[1] . "</a>"; ?></li>

				<?php endforeach; ?>
			</ul>
		</main>
	</body>
</html>
