<?php
if ($_SERVER['SERVER_PORT'] !== "443") {
	$location = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
	header("Location: " . $location);
	die();
}
?>
<div id="header">
	<a href="/">
<?php
$term_browsers = array("w3m", "Lynx", "Links", "ELinks");
$browser = get_browser(null, true);
$browser_term = in_array($browser['browser'], $term_browsers);
if (!$browser_term) {
	echo '<img src="/img/profile_picture_small.png" id="profile_pic">';
}
else {
	echo 'Home';
}
?>
	</a>
</div>
